# prueba-sourcemeridian

Prueba técnica Source Meridian

## Instalación

Para obtener el proyecto puede usar dos metodos. El primero consiste en descargar el archivo .zip directamente o pude usar el comando 
```
git clone https://gitlab.com/leoBAHM/prueba-sourcemeridian.git
```
Se recuerda que para ejecutar el anterior comando deberá tener instalado [git](https://git-scm.com/downloads) en el equipo en el cual
se ejecutará el proyecto.

## Creación de un ambiente virtual   

Con el objetivo de evitar conflictos entre las versiones de las librerías instaladas, es recomendable la creación de un ambiente virtual en la raíz del proyecto independiente. Use la librería de su preferencia para ello, en el caso particular se usa la librería [virtualenv](https://docs.python.org/es/3/library/venv.html).
```
cd prueba-sourcemeridian
virtualenv env
cd env/Scripts
activate
cd ../..
```
Con los anteriores comandos se crea y activa el ambiente virtual. Puede desactivarlo cuando desee con el comando 
```
deactive
```

## Instalación de librerías

Las librerías necesarias para el correcto fucionamiento del proyecto se encuentran el el archivo requirements.txt. Puede instalar manualmente cada una de estas o usar el comando  
```
pip install -r requirements.txt
```

Con esto quedará lista la configuración del equipo para la ejecución del proyecto.

# Contenido

El proyecto cuenta de una carpeta llamada models en el cual se encuentran los archivos encargados de Agregar, Cosultar, Actualizar y Eliminar los datos contenidos en las tablas que integran la base de datos. Adicional a ello, contará con un archivo con extensión .sql el cual contiene el script encargado de la creación de la base de datos así como sus tablas. Debe ejecutar este archivo con su gestor de dases de datos MySQL (recomiendo usar [MySQL Workbench](https://www.mysql.com/downloads/)) para la creación de la base de datos.

Los archivos con extensión .py cumplen funciones tales como validación de errores (errors.py) conección a la base de datos (db.py) e implementación de los algoritmos (main.py)

# Instrucciones de ejecución  

Antes de ejecutar el archivo main.py, configure los datos almacenados en el arcivo .env.
Dado que las credenciales de acceso al servidor de la base de datos varían de equipo en equipo este archivo es creado con la finalidad de seperar estas variables del código principal para que puedan ser modificadas facilmente sin que estas impacten en las líneas de código escritas. Deberá modificarlas como se sugiere a continuación:
```
[myEnv]
USER_DB=usuario de la base de datos (ejemplo: root)
PASSWORD_DB=contraseña de la base de datos
NAME_DB=nombre de la base de datos (El nombre de la base de datos creada con el script db.config.sql se llama food_recipes_db)
```

Es importante no dejar espacios antes y despues del "="

Una vez modificado el archivo .env, creada la base de datos y puesta en marcha del servicio de la base de datos, corra el archivo main.py

## Archivo main.py 

Corresponde al archivo que deberá ser ejecutado. Se creo con una estructura tipo menú para la creación, consulta, actualización y eliminación de los datos de una manera dinámica y sencilla para el usuario. La logica tras el funcionamiento del proyecto realmente se encuentra contenida en los demás archivos. 

Respecto a la opción número 5 del menú principal, se carga solo el nombre de los ingredientes almacenados en el archivo "ingr_map.pkl". Aunque el algoritmo es capaz de leer cualquier archivo en formato .pkl que contenga una columna de datos identificados con el header "raw_ingr". Para ver la lógica tras el almacenamiento masivo de información a partir de un archivo externo puede ver la funcióm load_ingredients en el archivo db.py

# Comentarios adicionales

Para la contrucción del modelo de la base de datos solo se tuvieron en cuenta arquellos datos que fueron considerados como pertinentes y cuya información no podría ser extraida usando querys combinados entre los distintos campos. En el archivo entidad_relacion.png podrá ver como es la relación entre las distintas tablas que contiene la base de datos.