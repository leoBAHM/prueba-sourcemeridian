from errors import CustomError
class User:
    """
    Class in charge of Create, Read, Update and Delete (CRUD) data of the table users.
    """
    def __init__(self, db):
        """
        Constructor of User class.

        Args:
            db --> mysql.connector.connection_cext.CMySQLConnection.
                Database connector used to perform queries
        """
        self.cnx = db
    
    def get(self, pk=None):
        """
        Method in charge of querying the users table using the element id or querying all the 
        elements of the table.
        
        Args:
            pk --> int or None.
                Element id 
        
        Returns:
            list of dicts or dict. Depends on whether or not the id of the element is sent.     
        """
        if pk:
            query = f"""SELECT * FROM users WHERE id={pk}"""
        else:
            query = """SELECT * FROM users"""
        
        cursor = self.cnx.cursor()
        try:
            cursor.execute(query)
            data = cursor.fetchall()
            info = [{"id": d[0], "names": d[1], "email": d[2], "created_date": d[3]} for d in data]
    
            if pk and info: return info[0]
            else: return info
        except Exception as e:
            raise CustomError(e.msg)
        
    def create(self, data):
        """ 
        Method in charge of creating new elements within the table users.
        
        Args:
            data --> list or tuple.
                New user information
        Returns:
            bool 
        """
        validation = self.validate(data)
        if validation:
            cursor = self.cnx.cursor()
            try:
                cursor.execute(("INSERT INTO  users (names, email) VALUES (%s, %s)"), data)
                self.cnx.commit()

                cursor.close()
                #self.cnx.close()
            except Exception as e:
                raise CustomError(e.msg)
                
        return True
    
    def update(self, instance, data):
        """ 
        Method in charge of updating elements within the table users.
        
        Args:
            data --> list or tuple.
                User information
        Returns:
            bool 
        """
        user = self.get(instance)
        if not user:
            raise CustomError("User not found")
        setInfo = set(user)
        setData = set(data) 
        SQL = f"UPDATE users SET names=%s, email=%s WHERE id={instance}"

        for name in setInfo.intersection(setData):
            user[name] = data[name]

        info = list(user.values())[1:-1]
        if self.validate(info):
            cursor = self.cnx.cursor()
            cursor.execute(SQL, info)  
            self.cnx.commit()
    
    def delete(self, pk):
        """ 
        Method in charge of deleting elements within the table users.
        
        Args:
            pk --> int.
                User id
        Returns:
            bool 
        """
        cursor = self.cnx.cursor()
        try:
            user = self.get(pk)
            if not user:
                raise CustomError("User not found")
            cursor.execute(f"DELETE FROM users WHERE id={pk}")
            self.cnx.commit()
            return True
        except CustomError as e:
            raise e
        except Exception as e:
            raise CustomError(e.msg)
    
    def validate(self, data):
        """
        Method in charge of validate the user information that wiil be added to the
        within the table users.
        
        Args:
            data --> list or tuple.
                New user information
        
        Args:
            data --> list or tuple.
                User information

        Raises:
            CustomError

        Returns:
            bool 
        """
        if not isinstance(data, (list, tuple)):
            raise CustomError(f"Invalid format for data.(names, email) expected but received {data}")
        if not isinstance(data[0], str):
            raise CustomError(f"Invalid format for names. Expected str")
        if not isinstance(data[1], str):
            raise CustomError(f"Invalid format for email. Expected str")
        if "@" not in data[1]:
            raise CustomError(f"Invalid format for email.")
        if "." not in data[1]:
            raise CustomError(f"Invalid format for email.")
        return True
        