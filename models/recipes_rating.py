from errors import CustomError
from models.users import User
class RecipesRating:
    """
    Class in charge of Create, Read, Update and Delete (CRUD) data of the 
    table recipes_rating.
    """
    def __init__(self, db):
        """
        Constructor of RecipesRating class.

        Args:
            db --> mysql.connector.connection_cext.CMySQLConnection.
                Database connector used to perform queries
        """
        self.cnx = db
    
    def get(self, pk=None):
        """
        Method in charge of querying the recipes_rating table using the element id or querying all the 
        elements of the table.
        
        Args:
            pk --> int or None.
                Element id 
        
        Returns:
            list of dicts or dict. Depends on whether or not the id of the element is sent.     
        """
        if pk:
            query = f"""SELECT * FROM recipe_rating WHERE id={pk}"""
        else:
            query = """SELECT * FROM recipe_rating"""
        
        cursor = self.cnx.cursor()
        try:
            cursor.execute(query)
            data = cursor.fetchall()
            info = [
                {
                    "id": str(d[0]), 
                    "user_id": str(d[1]),
                    "recipe_id": str(d[2]),
                    "rating": d[3],
                    "review": d[4]
                } for d in data
            ]
    
            if pk and info: return info[0]
            else: return info
        except Exception as e:
            raise CustomError(e.msg)
        
    def create(self, data):
        """ 
        Method in charge of creating new elements within the table recipes_rating.
        
        Args:
            data --> list or tuple.
                New recipes rating information
        Returns:
            bool 
        """
        validation = self.validate(data)
        if validation:
            cursor = self.cnx.cursor()
            try:
                cursor.execute(("INSERT INTO  recipe_rating (user_id,recipe_id,rating,review) VALUES (%s, %s, %s, %s)"), data)
                self.cnx.commit()

                cursor.close()
                #self.cnx.close()
            except Exception as e:
                raise CustomError(e.msg)
                
        return True
    
    def update(self, instance, data):
        """ 
        Method in charge of updating elements within the table recipes_rating.
        
        Args:
            data --> list or tuple.
                RecipesRating information
        Returns:
            bool 
        """
        recipe_rating = self.get(instance)
        if not recipe_rating:
            raise CustomError("recipe rating not found")

        setInfo = set(recipe_rating)
        setData = set(data)
        SQL = f"UPDATE recipe_rating SET user_id=%s, recipe_id=%s, rating=%s, review=%s WHERE id={instance}"

        for name in setInfo.intersection(setData):
            recipe_rating[name] = data[name]

        info = list(recipe_rating.values())[1:]
        if self.validate(info):
            cursor = self.cnx.cursor()
            cursor.execute(SQL, info)  
            self.cnx.commit()
    
    def delete(self, pk):
        """ 
        Method in charge of deleting elements within the table recipes_rating.
        
        Args:
            pk --> int.
                RecipesRating id
        Returns:
            bool 
        """
        cursor = self.cnx.cursor()
        try:
            user = self.get(pk)
            if not user:
                raise CustomError("recipe rating not found")
            cursor.execute(f"DELETE FROM recipe_rating WHERE id={pk}")
            self.cnx.commit()
            return True
        except CustomError as e:
            raise e
        except Exception as e:
            print(e)
            raise CustomError(e.msg)
    
    def validate(self, data):
        """
        Method in charge of validate the user information that wiil be added to the
        within the table recipes_rating.
        
        Args:
            data --> list or tuple.
                New  information
        
        Args:
            data --> list or tuple.
                RecipesRating information

        Raises:
            CustomError

        Returns:
            bool 
        """
        if not isinstance(data, (list, tuple)):
            raise CustomError(f"Invalid format for data.(user_id, recipe_id, rating, review) expected but received <{data}>")
        if not data[0].isdigit():
            raise CustomError(f"Invalid format for user_id. Expected digit")
        if not data[1].isdigit():
            raise CustomError(f"Invalid format for recipe_id. Expected digit")
        if not isinstance(data[2], str):
            raise CustomError(f"Invalid format for tags. Expected str")
        else:
            info = eval(data[2])
            if not isinstance(info, (int, float)):
                raise CustomError(f"Invalid element. Numeric type values ​​are expected")
        return True