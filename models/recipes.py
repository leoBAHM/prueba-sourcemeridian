from errors import CustomError
from models.ingredients import Ingredient
class Recipe:
    """
    Class in charge of Create, Read, Update and Delete (CRUD) data of the table recipe.
    """
    def __init__(self, db):
        """
        Constructor of Recipe class.

        Args:
            db --> mysql.connector.connection_cext.CMySQLConnection.
                Database connector used to perform queries
        """
        self.cnx = db
    
    def get(self, pk=None):
        """
        Method in charge of querying the recipe table using the element id or querying all the 
        elements of the table.
        
        Args:
            pk --> int or None.
                Element id 
        
        Returns:
            list of dicts or dict. Depends on whether or not the id of the element is sent.     
        """
        if pk:
            query = f"""SELECT * FROM recipe WHERE id={pk}"""
        else:
            query = """SELECT * FROM recipe"""
        
        cursor = self.cnx.cursor()
        try:
            cursor.execute(query)
            data = cursor.fetchall()
            info = [
                {
                    "id": str(d[0]), 
                    "user_id": str(d[1]),
                    "name": d[2], 
                    "minutes": d[3],
                    "tags": d[4],
                    "nutrition": d[5],
                    "num_steps": d[6],
                    "steps": d[7],
                    "description": d[8],
                    "ingredients": d[9],
                    "submit": d[10], 
                } for d in data
            ]
    
            if pk and info: return info[0]
            else: return info
        except Exception as e:
            raise CustomError(e.msg)
        
    
    def create(self, data):
        """ 
        Method in charge of creating new elements within the table recipe.
        
        Args:
            data --> list or tuple.
                New recipe information
        Returns:
            bool 
        """
        validation = self.validate(data)
        if validation:
            cursor = self.cnx.cursor()
            try:
                cursor.execute(("INSERT INTO  recipe (user_id,name,minutes,tags,nutricion,num_steps,steps,description,ingredients) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"), data)
                self.cnx.commit()

                cursor.close()
                #self.cnx.close()
            except Exception as e:
                raise CustomError(e.msg)
                    
        return True
    
    def update(self, instance, data):
        """ 
        Method in charge of updating elements within the table recipe.
        
        Args:
            data --> list or tuple.
                Recipe information
        Returns:
            bool 
        """
        recipe = self.get(instance)
        if not recipe:
            raise CustomError("recipe not found")
        setInfo = set(recipe)
        setData = set(data)
        SQL = f"UPDATE recipe SET user_id=%s, name=%s, minutes=%s, tags=%s, nutricion=%s, num_steps=%s, steps=%s, description=%s, ingredients=%s WHERE id={instance}"
        for name in setInfo.intersection(setData):
            recipe[name] = data[name] 
        info = list(recipe.values())[1:-1]
        if self.validate(info):
            cursor = self.cnx.cursor()
            cursor.execute(SQL, info)  
            self.cnx.commit()  
    
    def delete(self, pk):
        """ 
        Method in charge of deleting elements within the table recipe.
        
        Args:
            pk --> int.
                Recipe id
        Returns:
            bool 
        """
        cursor = self.cnx.cursor()
        try:
            user = self.get(pk)
            if not user:
                raise CustomError("recipe not found")
            cursor.execute(f"DELETE FROM recipe WHERE id={pk}")
            self.cnx.commit()
            return True
        except CustomError as e:
            raise e
        except Exception as e:
            raise CustomError(e.msg)
    
    def validate(self, data):
        """
        Method in charge of validate the user information that wiil be added to the
        within the table recipe.
        
        Args:
            data --> list or tuple.
                New recipe information
        
        Args:
            data --> list or tuple.
                Recipe information

        Raises:
            CustomError

        Returns:
            bool 
        """
        if not isinstance(data, (list, tuple)):
            raise CustomError(f"Invalid format for data.(user_id, name, minutes, tags, nutrition, num_steps, steps, description, ingredients) expected but received <{data}>")
        if not data[0].isdigit():
            raise CustomError(f"Invalid format for user_id. Expected digit")
        if not isinstance(data[1], str):
            raise CustomError(f"Invalid format for name. Expected str")
        if not data[2].isdigit():
            raise CustomError(f"Invalid format for minutes. Expected digit")
        if not isinstance(data[3], str):
            raise CustomError(f"Invalid format for tags. Expected str")
        else: 
            info = eval(data[3])
            if not isinstance(info, list):
                raise CustomError(f"Invalid format for tags. Expected str with format <['tag1', 'tag2', ... 'tagn']>")
        if not isinstance(data[4], str):
            raise CustomError(f"Invalid format for nutricion. Expected str")
        else: 
            info = eval(data[4])
            if not isinstance(info, list):
                raise CustomError(f"Invalid format for nutricion. Expected str with format <[calories, total fat, sugar, sodium, protein, saturated fat, carbohydrates]>")
            else: 
                if len(info) != 7:
                    raise CustomError(f"Invalid element number. Values expected for <[calories, total fat, sugar, sodium, protein, saturated fat, carbohydrates]>")
                if not all(isinstance(s, (int, float)) for s in info):
                    raise CustomError(f"Invalid element. Numeric type values ​​are expected for <[calories, total fat, sugar, sodium, protein, saturated fat, carbohydrates]>")
        if not data[5].isdigit():
            raise CustomError(f"Invalid format for num_steps. Expected digit")
        if not isinstance(data[6], str):
            raise CustomError(f"Invalid format for tags. Expected str")
        else: 
            info = eval(data[6])
            if not isinstance(info, list):
                raise CustomError(f"Invalid format for tags. Expected str with format <['step1', 'step2', ... 'stepn']>")
        if eval(data[5]) != len(info):
            raise CustomError(f"Invalid element number for steps. The number of elements must be the same as <num_steps>")
        if not isinstance(data[8], str):
            raise CustomError(f"Invalid format for ingredients. Expected str")
        else: 
            info = eval(data[8])
            ingredients = Ingredient(self.cnx)
            for i in info:
                if not ingredients.get(i):
                    raise CustomError(f"Ingredient with id {i} not found")
            
        return True