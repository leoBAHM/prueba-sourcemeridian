from errors import CustomError
class Ingredient:
    """
    Class in charge of Create, Read, Update and Delete (CRUD) data of the table ingredients.
    """
    def __init__(self, db):
        """
        Constructor of Ingredients class.

        Args:
            db --> mysql.connector.connection_cext.CMySQLConnection.
                Database connector used to perform queries
        """
        self.cnx = db
    
    def get(self, pk=None):
        """
        Method in charge of querying the ingredients table using the element id or querying all the 
        elements of the table.
        
        Args:
            pk --> int or None.
                Element id 
        
        Returns:
            list of dicts or dict. Depends on whether or not the id of the element is sent.     
        """
        if pk:
            query = f"""SELECT * FROM ingredients WHERE id={pk}"""
        else:
            query = """SELECT * FROM ingredients"""
        
        cursor = self.cnx.cursor()
        try:
            cursor.execute(query)
            data = cursor.fetchall()
            info = [{"id": d[0], "names": d[1]} for d in data]
    
            if pk and info: return info[0]
            else: return info
        except Exception as e:
            raise CustomError(e.msg)
        
    def create(self, data): 
        """ 
        Method in charge of creating new elements within the table ingredients.
        
        Args:
            data --> list or tuple.
                New ingredients information
        Returns:
            bool 
        """
        cursor = self.cnx.cursor()
        try:
            data = [data]
            cursor.execute(("INSERT INTO ingredients (name) VALUES(%s)"), data)
            self.cnx.commit()

            cursor.close()
            #self.cnx.close()
        except Exception as e:
            raise CustomError(e.msg)
                
        return True
    
    def update(self, instance, data):
        """ 
        Method in charge of updating elements within the table ingredients.
        
        Args:
            data --> list or tuple.
                Ingredient information
        Returns:
            bool 
        """
        SQL = f"UPDATE ingredients SET name=%s WHERE id = {instance}"
        data = [data]
        cursor = self.cnx.cursor()
        cursor.execute(SQL, data)  
        self.cnx.commit()  
    
    def delete(self, pk):
        """ 
        Method in charge of deleting elements within the table ingredients.
        
        Args:
            pk --> int.
                Ingredient id
        Returns:
            bool 
        """
        cursor = self.cnx.cursor()
        try:
            ingredient = self.get(pk)
            if not ingredient:
                raise CustomError("Ingredient not found")
            cursor.execute(f"DELETE FROM ingredients WHERE id={pk}")
            self.cnx.commit()
            return True
        except CustomError as e:
            raise e
        except Exception as e:
            raise CustomError(e.msg)
    
    def validate(self, data):
        """
        Method in charge of validate the user information that wiil be added to the
        within the table ingredients.
        
        Args:
            data --> list or tuple.
                New ingredients information
        
        Args:
            data --> list or tuple.
                User information

        Raises:
            CustomError

        Returns:
            bool 
        """
        if not isinstance(data, str):
            raise CustomError(f"Invalid format for names. Expected str")
        return True