import os
import configparser

import pickle
import pandas as pd
import mysql.connector
from errors import CustomError


config = configparser.ConfigParser()
config.read('.env')
index = config['myEnv']

def connect():    
    cnx = mysql.connector.connect(user=index["USER_DB"], password=index["PASSWORD_DB"], database=index["NAME_DB"])
    return cnx

def load_ingredients(cnx, path):
    """
    Uploading files in .pkl format

    Args:
        db --> mysql.connector.connection_cext.CMySQLConnection.
                Database connector used to perform queries
        path --> str  
                File path  

    Raises:
        CustomError
    
    Returns:
        bool
    """
    info, extension = os.path.splitext(path)
    if extension == ".pkl":
        try:
            with open(path, 'rb') as f:
                data = pickle.load(f)
            df = pd.DataFrame(data._data)
            data = df['raw_ingr'].to_list()
            data_ = [[d] for d in data]

            cursor = cnx.cursor()
            cursor.executemany("INSERT INTO ingredients (name) VALUES(%s)", data_)
            cnx.commit()

            cursor.close()
            cnx.close()
            return True
        except:
            raise CustomError("File could not be uploaded")
    else:
        raise CustomError(f"Invalid format for file")

