from db import connect, load_ingredients
from models.users import User
from models.ingredients import Ingredient
from models.recipes import Recipe
from models.recipes_rating import RecipesRating
from errors import CustomError



cnx = connect() # connection to database

def commun_display(title):
    print(f"""
        {title}
        
        Opciones:
        
        1.- Crear
        2.- consultar
        3.- Actualizar
        4.- Eliminar
        5.- Menú Principal
        
        """)
    opc = input("Ingrese la opción: ")
    while True:
        if opc not in ['1', '2', '3', '4', '5']:
            print("¡ERROR! Ingrese una opción valida", end="\n\n")
            opc = input("Ingrese la opción: ")
        else: 
            break
    return opc
            
def user_menu():
    user = User(cnx)
    while True:
        opc = commun_display("Menú de usuarios")
        if opc == "1":
            name = input("Ingrese el nombre: ")
            email = input("Ingrese el email: ")
            try:
                user.create([name, email])
                print("Usuario Creado")
            except Exception as e:
                print('¡ERROR!', e)
        elif opc == "2":
            pk = input("Ingrese id del usuario o presione enter para cosultar todos: ")
            if pk:
                try: 
                    pk = int(pk)
                    print(user.get(pk))
                except CustomError as e:
                    print(e)
                except:
                    print("¡ERROR! El id debe ser de tipo numérico")
            else: print(user.get())
        elif opc == "3":
            pk = input("Ingrese el id del usuario: ")
            names = input("Ingrese el nuevo nombre o enter si quiere conserver el anterior: ")
            email = input("Ingrese el nuevo email o enter si quiere conserver el anterior: ")
            data = {"names": names, "email": email}
            query = {k: v for k, v in data.items() if v !=""}
            if pk.isdigit():
                try:
                    user.update(int(pk), query)
                    print(f"Usuario con id {pk} actualizado")
                except Exception as e:
                    print(e)
        elif opc == "4":
            pk = input("Ingrese el id del usuario: ")
            if pk.isdigit():
                try:
                    user.delete(int(pk))
                    print(f"Usuario con id {pk} eliminado")
                except Exception as e:
                    print(e)
            else: print("¡Error! El id ingresado debe ser de tipo numérico")    
        elif opc == '5':
            break

def set_data_recipes(update=None):
    pk=[]
    if update:
        pk = input("Ingrese el id de la receta: ")
    else: update=""
    user_id = input(f"Ingrese el id del usuario {update}: ")
    name = input(f"Ingrese el nombre {update}: ")
    minutes = input(f"Ingrese el tiempo estimado de preparación [min] {update}: ")
    tags = input(f"Ingrese las etiquetas de identificación (list) {update}: ")
    nutrition = input(f"""Ingrese valores nutricionales así:
                        [calories, total fat, sugar, sodium, protein, saturated fat, carbohydrates] {update}: """)
    num_steps = input(f"Número de pasos {update}: ")
    steps = input(f"Ingrese los pasos para preparar la receta (list) {update}: ")
    description = input(f"Ingrese la descripción de la receta {update}: ")
    ingredients = input(f"Ingrese los id de los ingredientes que integran la receta (list) {update}: ")
    data = [user_id, name, minutes, tags, nutrition, num_steps, steps, description, ingredients]
    if pk: data.insert(0, pk)
    return data

def recipe_menu():
    recipe = Recipe(cnx)
    while True:
        opc = commun_display("Menú de recetas")
        if opc == '1':
            data = set_data_recipes()
            try:
                recipe.create(data)
                print("Receta agregada con éxito")
            except Exception as e:
                print(e)
        elif opc == "2":
            pk = input("Ingrese id de la receta o presione enter para cosultar todas: ")
            if pk:
                try: 
                    pk = int(pk)
                    print(recipe.get(pk))
                except CustomError as e:
                    print(e)
                except:
                    print("¡ERROR! El id debe ser de tipo numérico")
            else: print(recipe.get())
        elif opc == "3":
            data = set_data_recipes(update="o enter si quiere conserver el anterior")
            pk = data[0]
            info = {
                "user_id": data[1],
                "name": data[2],
                "minutes": data[3],
                "tags": data[4],
                "nutrition": data[5],
                "num_steps": data[6],
                "steps": data[7],
                "description": data[8],
                "ingredients": data[9],
            }
            query = {k: v for k, v in info.items() if v !=""}
            if pk.isdigit():
                try:
                    recipe.update(int(pk), query)
                    print(f"Receta con id {pk} actualizado")
                except Exception as e:
                    print(e)
        elif opc == "4":
            pk = input("Ingrese el id de la receta: ")
            if pk.isdigit():
                try:
                    recipe.delete(int(pk))
                    print(f"La receta con id {pk} eliminado")
                except Exception as e:
                    print(e)
            else: print("¡Error! El id ingresado debe ser de tipo numérico")
        elif opc == '5':
            break

def set_data_recipes_rating(update=None):
    pk=[]
    if update:
        pk = input("Ingrese el id de la receta valorada: ")
    else: update=""
    user_id = input(f"Ingrese el id del usuario {update}: ")
    recipe_id = input(f"Ingrese el id de la receta {update}: ")
    rating = input(f"Ingrese la calificación {update}: ")
    review = input(f"Ingrese el comentario {update}: ")
    
    data = [user_id, recipe_id, rating, review]
    if pk: data.insert(0, pk)
    return data

def recipe_rating_menu():
    recipe = RecipesRating(cnx)
    while True:
        opc = commun_display("Menú de valorar recetas")  
        if opc == '1':
            data = set_data_recipes_rating()
            try:
                recipe.create(data)
                print("Valoración de la receta agregada con éxito")
            except Exception as e:
                print(e)
        elif opc == "2":
            pk = input("Ingrese id de la valoración de la receta o presione enter para cosultar todas: ")
            if pk:
                try: 
                    pk = int(pk)
                    print(recipe.get(pk))
                except CustomError as e:
                    print(e)
                except:
                    print("¡ERROR! El id debe ser de tipo numérico")
            else: print(recipe.get())
        elif opc == "3":
            data = set_data_recipes_rating(update="o enter si quiere conserver el anterior")
            pk = data[0]
            info = {
                "user_id": data[1],
                "recipe_id": data[2],
                "rating": data[3],
                "review": data[4]
            }
            query = {k: v for k, v in info.items() if v !=""}
            if pk.isdigit():
                try:
                    recipe.update(int(pk), query)
                    print(f"Valoracion de receta con id {pk} actualizado")
                except Exception as e:
                    print(e)
        elif opc == "4":
            pk = input("Ingrese el id de la valoracion de la receta: ")
            if pk.isdigit():
                try:
                    recipe.delete(int(pk))
                    print(f"La valoracion de la receta con id {pk} eliminada")
                except Exception as e:
                    print(e)
            else: print("¡Error! El id ingresado debe ser de tipo numérico")
        elif opc == '5':
            break

def ingredients_menu():
    recipe = Ingredient(cnx)
    while True:
        opc = commun_display("Menú de ingredientes")  
        if opc == '1':
            data = input("Ingrese el nombre del ingrediente: ")
            try:
                recipe.create(data)
                print("Ingrediente agregado con éxito")
            except Exception as e:
                print(e)
        elif opc == "2":
            pk = input("Ingrese id del ingrediente o presione enter para cosultar todos: ")
            if pk:
                try: 
                    pk = int(pk)
                    print(recipe.get(pk))
                except CustomError as e:
                    print(e)
                except:
                    print("¡ERROR! El id debe ser de tipo numérico")
            else: print(recipe.get())
        elif opc == "3":
            pk = input("Ingrese el id del ingrediente: ")
            data = input("Ingrese el nombre del ingrediente: ")
            
            if pk.isdigit() and data:
                try:
                    recipe.update(int(pk), data)
                    print(f"Ingrediente con id {pk} actualizado")
                except Exception as e:
                    print(e)
        elif opc == "4":
            pk = input("Ingrese el id del ingrediente: ")
            if pk.isdigit():
                try:
                    recipe.delete(int(pk))
                    print(f"el ingrediente con id {pk} eliminado")
                except Exception as e:
                    print(e)
            else: print("¡Error! El id ingresado debe ser de tipo numérico")
        elif opc == '5':
            break

def main():
    while True:
        print("""
              Implementtación API prueba técnica SourceMeridian.  
              
              
              Opciones:
              
              1.- Menú Usuarios.
              2.- Agregar Receta
              3.- Valorar Receta 
              4.- Agregar Ingredientes
              5.- Cargar ingredientes de archivo .pkl
              6.- Salir
        
              """)
        opc = input("Ingrese la opción: ")
        while True:
            if opc not in ['1', '2', '3', '4', '5', '6']:
                print("¡ERROR! Ingrese una opción valida", end="\n\n")
                opc = input("Ingrese la opción: ")
            else: 
                break
        if opc == "1":
            user_menu()
        elif opc == "2":
            recipe_menu()
        elif opc == "3":
            recipe_rating_menu()  
        elif opc == "4":
            ingredients_menu() 
        elif opc == "5":
            path = input("Ingrese la ruta del archivo .pkl: ") 
            try:
                load_ingredients(cnx, path)
                print("Ingredients cargados")
            except Exception as e:
                print(e)
        if opc == '6':
            print("Hasta pronto", end="\n\n")
            break

if __name__ == "__main__":
    main()

