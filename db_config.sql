/*
Scripts encargado de la creación de la base de datos y las tablas que la componen.

En total son 4 tablas. 

users: almacena cuentas de usuarios los cueles interactuan con 
la aplicación para agregar recetas y calificar las recetas existentes.  

ingredients: almacena el nombre de los ingredientes usados en cada una de las recetas.

recipe: almacena las recetas en cuestión. Posee varios campos los cuales almacenaran un 
string con formato de listas (array) que contendrán comentarios, id de ingredientes,
pasos, etc.

recipe_rating: contiene la caificación que un usuario le da a una receta, así como un
comentario asociado a este. 
*/
# Se crea la base de datos con nombre *food_recipes_db*
CREATE database food_recipes_db;

# Se crea una tabla dentro de la base datos *users*
CREATE table food_recipes_db.users(
	id bigint PRIMARY KEY auto_increment,
    names varchar(50) not null,
    email varchar(30) not null unique,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP # fecha actual por defecto
);

# Se crea una tabla dentro de la base datos *ingredient*
CREATE TABLE food_recipes_db.ingredients (
    id bigint PRIMARY KEY auto_increment,
    name VARCHAR(255) NOT NULL unique
);

# Se crea una tabla dentro de la base datos *recipe*
CREATE table food_recipes_db.recipe(
	id bigint PRIMARY KEY auto_increment,
    user_id bigint not null, # fk de table usuario
    name varchar(255) not null,
    minutes varchar(4) not null,
    tags text not null, # str con formato lista que contine los tags
    nutricion text not null, # str con formato lista que contine los valores nutricionales
    num_steps varchar(3), # Varchar debido a que no se efectuaran operaciones matemáticas con este valor
    steps text not null,# str con formato lista que contine los pasos
    description text not null,
    ingredients text not null, # str con formato lista que contine los ids de los ingredientes
    submit TIMESTAMP DEFAULT CURRENT_TIMESTAMP, # fecha actual por defecto
    foreign key (user_id) references food_recipes_db.users(id)
);

# Se crea una tabla dentro de la base datos *recipe_rating*
CREATE table food_recipes_db.recipe_rating(
	id bigint PRIMARY KEY auto_increment, 
    user_id bigint not null, # fk de table usuario
    recipe_id bigint not null, # fk de table recipe
    rating varchar(3), # Varchar debido a que no se efectuaran operaciones matemáticas con este valor
    review text not null, 
    submit TIMESTAMP DEFAULT CURRENT_TIMESTAMP, # fecha actual por defecto
    foreign key (user_id) references food_recipes_db.users(id),
    foreign key (recipe_id) references food_recipes_db.recipe(id)
);